import java.util.Arrays;

public class GameLogic {
	
	private LogicBoard[][] board = new LogicBoard[8][8];
	private int opponentColor;
	private int playerColor;
	private int[] message;
	private int[] newUpdate = null;
	private int updateCounter = 2;
	private int whiteScore = 2;
	private int blackScore = 2;
	private int possibleMoves;
	
	public GameLogic(int color) {
		for(int i=0; i<8; i++) {
			for(int j=0; j<8; j++) {
				board[i][j] = new LogicBoard();
			}
		}
		if(color == 1) {
			opponentColor = 2;
			playerColor = 1;
		}
		else {
			opponentColor = 1;
			playerColor = 2;
		}
	
		board[3][3].setStatus(1);
		board[4][4].setStatus(1);
		board[3][4].setStatus(2);
		board[4][3].setStatus(2);
		
		/*
		board[0][0].setStatus(2);
		//board[0][1].setStatus(1);
		board[0][2].setStatus(1);
		board[0][3].setStatus(2);
		board[0][4].setStatus(1);
		board[0][5].setStatus(1);
		board[0][6].setStatus(2);
		board[0][7].setStatus(2);

		//board[1][0].setStatus(2);
		board[1][1].setStatus(1);//board[1][1].setStatus(1);
		board[1][2].setStatus(1);//board[1][2].setStatus(1);
		board[1][3].setStatus(2);
		board[1][4].setStatus(1);
		board[1][5].setStatus(1);
		board[1][6].setStatus(1);
		board[1][7].setStatus(1);

		board[2][0].setStatus(1);
		board[2][1].setStatus(1);//board[2][1].setStatus(1);
		board[2][2].setStatus(2);
		board[2][3].setStatus(1);
		board[2][4].setStatus(2);
		board[2][5].setStatus(1);
		board[2][6].setStatus(2);
		board[2][7].setStatus(1);

		board[3][0].setStatus(1);
		board[3][1].setStatus(1);
		board[3][2].setStatus(1);
		board[3][3].setStatus(2);
		board[3][4].setStatus(2);
		board[3][5].setStatus(1);
		board[3][6].setStatus(2);
		board[3][7].setStatus(2);

		board[4][0].setStatus(1);
		board[4][1].setStatus(1);
		board[4][2].setStatus(1);
		board[4][3].setStatus(1);
		board[4][4].setStatus(1);
		board[4][5].setStatus(2);
		board[4][6].setStatus(2);
		board[4][7].setStatus(2);

		board[5][0].setStatus(1);
		board[5][1].setStatus(1);
		board[5][2].setStatus(2);
		board[5][3].setStatus(1);
		board[5][4].setStatus(2);
		board[5][5].setStatus(2);
		board[5][6].setStatus(2);
		board[5][7].setStatus(2);

		board[6][0].setStatus(1);
		board[6][1].setStatus(1);
		board[6][2].setStatus(1);
		board[6][3].setStatus(2);
		board[6][4].setStatus(2);
		board[6][5].setStatus(2);
		board[6][6].setStatus(1);
		board[6][7].setStatus(2);

		board[7][0].setStatus(1);
		board[7][1].setStatus(1);
		board[7][2].setStatus(2);
		board[7][3].setStatus(2);
		board[7][4].setStatus(2);
		board[7][5].setStatus(2);
		board[7][6].setStatus(2);
		board[7][7].setStatus(2);
		*/
	}
	
	public int getPlayerColor() {
		return playerColor;
	}
	
	public int getOpponentColor() {
		return opponentColor;
	}
	
	public void setMessage(int[] message) {
		this.message = message;
	}
	
	public int[] getMessage() {
		return message;
	}
	
	public void updateBoard(int[] message, int color) {
		for(int i=0; i<message.length; i+=2) {
			board[message[i]][message[i+1]].setStatus(color);
		}
	}

	public void setScore(int scoreIncrement, boolean isOpponentMove) {
		if(isOpponentMove == false) {
			if(playerColor == 1) {
				this.whiteScore = this.whiteScore + scoreIncrement;
				this.blackScore = this.blackScore - (scoreIncrement - 1);
			}
			else {
				this.blackScore = this.blackScore + scoreIncrement;
				this.whiteScore = this.whiteScore - (scoreIncrement - 1);
			}
		}
		else {
			if(playerColor == 1) {
				this.blackScore = this.blackScore + scoreIncrement;
				this.whiteScore = this.whiteScore - (scoreIncrement - 1);
			}
			else {
				this.whiteScore = this.whiteScore + scoreIncrement;
				this.blackScore = this.blackScore - (scoreIncrement - 1);
			}
		}
	}
	
	public int getWhiteScore() {
		return whiteScore;
	}
	
	public int getBlackScore() {
		return blackScore;
	}


	public int[] getValidMoves(int row, int col) {
		return board[row][col].getValidMoves();
	}
	
	public int[] isValidMove(int row, int col, boolean checkPlayer) {

		int[] coordinate = {row, col};
		int[] east = checkEast(row, col, checkPlayer);
		int[] west = checkWest(row, col, checkPlayer);
		int[] north = checkNorth(row, col, checkPlayer);
		int[] south = checkSouth(row, col, checkPlayer);
		int[] northeast = checkNorthEast(row, col, checkPlayer);
		int[] northwest = checkNorthWest(row, col, checkPlayer);
		int[] southeast = checkSouthEast(row, col, checkPlayer);
		int[] southwest = checkSouthWest(row, col, checkPlayer);
		
		int[] merge = merge(east, west, north, south, northeast, northwest, southeast, southwest);
 		if(merge.length == 0) return null;
		else {
			int[] flipPieces = new int[merge.length+2];
			System.arraycopy(merge, 0, flipPieces, 0, merge.length);
			flipPieces[flipPieces.length-2] = row;
			flipPieces[flipPieces.length-1] = col;
			return flipPieces;
		}
		
		
	}
	
	public int getPossibleMoves() {
		return possibleMoves;
	}
	
	public int checkPossibleMoves() {
		possibleMoves = 0;
		for(int i=0; i<8; i++) {
			for(int j=0; j<8; j++) {
				if(board[i][j].getStatus() == 0) {
					int[] pieces = isValidMove(i, j, true); 
 					if(pieces != null) {
						board[i][j].setValidMoves(pieces);
						possibleMoves++;
					}
 					else {
 						board[i][j].setValidMoves(null);
 					}
				}
				else {
					board[i][j].setValidMoves(null);
				}
			}
		}
		return possibleMoves;
	}
	
	public int checkOpponentMoves() {
		int possibleMoves = 0;
		for(int i=0; i<8; i++) {
			for(int j=0; j<8; j++) {
				if(board[i][j].getStatus() == 0) {
					int[] pieces = isValidMove(i, j, false);
					if(pieces != null) {
						possibleMoves++;
					}
				}
			}
		}
		return possibleMoves;
	}
	
	//Check right of piece
	public int[] checkEast(int row, int col, boolean player) {
		int pieceColor = opponentColor;
		if(player == false) pieceColor = playerColor;
		if(col<6 && board[row][col+1].getStatus() == pieceColor) {
			boolean isValid = false;
			int nextCol = col+2;
			
			while(isValid==false && nextCol<8) {
				if(board[row][nextCol].getStatus() == pieceColor) {
					nextCol++;
				}
				else if(board[row][nextCol].getStatus() != 0) {
					isValid = true;
					int colCount = col+1;
					int[] update = new int[(nextCol-colCount)*2];
					for(int i=0; colCount<nextCol; i+=2) {
						update[i] = row;
						update[i+1] = colCount;
						colCount++;
					}
					return update;
				}
				else break;
			}
		}
		
		int[] noUpdate = new int[0];
		return noUpdate;
	}
	
	//Check left of piece
	public int[] checkWest(int row, int col, boolean player) {
		int pieceColor = opponentColor;
		if(player == false) pieceColor = playerColor;
		if(col>1 && board[row][col-1].getStatus() == pieceColor) {
			boolean isValid = false;
			int nextCol = col-2;

			while(isValid==false && nextCol>-1) {
				if(board[row][nextCol].getStatus() == pieceColor) {
					nextCol--;
				}
				else if(board[row][nextCol].getStatus() != 0) {
					isValid = true;
					int colCount = col-1;
					int[] update = new int[(colCount-nextCol)*2];
					for(int i=0; colCount>nextCol; i+=2) {
						update[i] = row;
						update[i+1] = colCount;
						colCount--;
					}
					return update;
				}
				else break;
			}
		}

		int[] noUpdate = new int[0];
		return noUpdate;
	}

	//Check north of piece
	public int[] checkNorth(int row, int col, boolean player) {
		int pieceColor = opponentColor;
		if(player == false) pieceColor = playerColor;
		if(row>1 && board[row-1][col].getStatus() == pieceColor) {
			boolean isValid = false;
			int nextRow = row-2;

			while(isValid==false && nextRow>-1) {
				if(board[nextRow][col].getStatus() == pieceColor) {
					nextRow--;
				}
				else if(board[nextRow][col].getStatus() != 0) {
					isValid = true;
					int rowCount = row-1;
					int[] update = new int[(rowCount-nextRow)*2];
					for(int i=0; rowCount>nextRow; i+=2) {
						update[i] = rowCount;
						update[i+1] = col;
						rowCount--;
					}
					return update;
				}
				else break;
			}
		}

		int[] noUpdate = new int[0];
		return noUpdate;
	}
	
	//Check south of piece
	public int[] checkSouth(int row, int col, boolean player) {
		int pieceColor = opponentColor;
		if(player == false) pieceColor = playerColor;
		if(row<6 && board[row+1][col].getStatus() == pieceColor) {
			boolean isValid = false;
			int nextRow = row+2;

			while(isValid==false && nextRow<8) {
				if(board[nextRow][col].getStatus() == pieceColor) {
					nextRow++;
				}
				else if(board[nextRow][col].getStatus() != 0) {
					isValid = true;
					int rowCount = row+1;
					int[] update = new int[(nextRow-rowCount)*2];
					for(int i=0; rowCount<nextRow; i+=2) {
						update[i] = rowCount;
						update[i+1] = col;
						rowCount++;
					}
					return update;
				}
				else break;
			}
		}

		int[] noUpdate = new int[0];
		return noUpdate;
	}
	
	//Check NW of piece
	public int[] checkNorthWest(int row, int col, boolean player) {
		int pieceColor = opponentColor;
		if(player == false) pieceColor = playerColor;
		if(col>1 && row>1 && board[row-1][col-1].getStatus() == pieceColor) {
			boolean isValid = false;
			int nextRow = row-2;
			int nextCol = col-2;

			while(isValid==false && nextRow>-1 && nextCol>-1) {
				if(board[nextRow][nextCol].getStatus() == pieceColor) {
					nextRow--;
					nextCol--;
				}
				else if(board[nextRow][nextCol].getStatus() != 0) {
					isValid = true;
					int rowCount = row-1;
					int colCount = col-1;
					int[] update = new int[(rowCount-nextRow)*2];
					for(int i=0; rowCount>nextRow && colCount>nextCol; i+=2) {
						update[i] = rowCount;
						update[i+1] = colCount;
						rowCount--;
						colCount--;
					}
					return update;
				}
				else break;
			}
		}

		int[] noUpdate = new int[0];
		return noUpdate;
	}
	
	//Check NE of piece
	public int[] checkNorthEast(int row, int col, boolean player) {
		int pieceColor = opponentColor;
		if(player == false) pieceColor = playerColor;
		if(col<6 && row>1 && board[row-1][col+1].getStatus() == pieceColor) {
			boolean isValid = false;
			int nextRow = row-2;
			int nextCol = col+2;

			while(isValid==false && nextRow>-1 && nextCol<8) {
				if(board[nextRow][nextCol].getStatus() == pieceColor) {
					nextRow--;
					nextCol++;
				}
				else if(board[nextRow][nextCol].getStatus() != 0) {
					isValid = true;
					int rowCount = row-1;
					int colCount = col+1;
					int[] update = new int[(rowCount-nextRow)*2];
					for(int i=0; rowCount>nextRow && colCount<nextCol; i+=2) {
						update[i] = rowCount;
						update[i+1] = colCount;
						rowCount--;
						colCount++;
					}
					return update;
				}
				else break;
			}
		}

		int[] noUpdate = new int[0];
		return noUpdate;
	}
	
	//Check SW of piece
	public int[] checkSouthWest(int row, int col, boolean player) {
		int pieceColor = opponentColor;
		if(player == false) pieceColor = playerColor;
		if(col>1 && row<6 && board[row+1][col-1].getStatus() == pieceColor) {
			boolean isValid = false;
			int nextRow = row+2;
			int nextCol = col-2;

			while(isValid==false && nextRow<8 && nextCol>-1) {
				if(board[nextRow][nextCol].getStatus() == pieceColor) {
					nextRow++;
					nextCol--;
				}
				else if(board[nextRow][nextCol].getStatus() != 0) {
					isValid = true;
					int rowCount = row+1;
					int colCount = col-1;
					int[] update = new int[(nextRow-rowCount)*2];
					for(int i=0; rowCount<nextRow && colCount>nextCol; i+=2) {
						update[i] = rowCount;
						update[i+1] = colCount;
						rowCount++;
						colCount--;
					}
					return update;
				}
				else break;
			}
		}

		int[] noUpdate = new int[0];
		return noUpdate;
	}
	
	//Check SE of piece
	public int[] checkSouthEast(int row, int col, boolean player) {
		int pieceColor = opponentColor;
		if(player == false) pieceColor = playerColor;
		if(col<6 && row<6 && board[row+1][col+1].getStatus() == pieceColor) {
			boolean isValid = false;
			int nextRow = row+2;
			int nextCol = col+2;

			while(isValid==false && nextRow<8 && nextCol<8) {
				if(board[nextRow][nextCol].getStatus() == pieceColor) {
					nextRow++;
					nextCol++;
				}
				else if(board[nextRow][nextCol].getStatus() != 0) {
					isValid = true;
					int rowCount = row+1;
					int colCount = col+1;
					int[] update = new int[(nextRow-rowCount)*2];
					for(int i=0; rowCount<nextRow && colCount<nextCol; i+=2) {
						update[i] = rowCount;
						update[i+1] = colCount;
						rowCount++;
						colCount++;
					}
					return update;
				}
				else break;
			}
		}

		int[] noUpdate = new int[0];
		return noUpdate;
	}
	

	public int[] merge(int[]...arrays) {
		int count = 0;
		for(int[] array : arrays) {
			count+=array.length;
		}
		int[] merge = new int[count];

		int index = 0;
		for(int[] array : arrays) {
				System.arraycopy(array, 0, merge, index, array.length);
				index+=array.length;
		}
		return merge;
	}

	
}






	
	
	
	
	
	

