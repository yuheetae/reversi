import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class HighlightSquare implements MouseListener{

	
	private boolean valid = false;
	private Color highlightYellow = new Color(255,255,102);
	private Color reversiGreen = new Color(57, 126, 88);

	@Override
	public void mouseEntered(MouseEvent e) {
		if(valid == false) {
			return;
		}
		else {
			e.getComponent().setBackground(highlightYellow);
		}
		
	}
	
	public void setValid(boolean valid) {
		this.valid = valid;
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if(valid == false) {
			return;
		}
		else {
			e.getComponent().setBackground(reversiGreen);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
