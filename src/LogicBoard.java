
public class LogicBoard {
	
	private int status = 0;
	private int[] validMoves;

	public LogicBoard() {
		this.status = 0;
		this.validMoves = null;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setValidMoves(int[] validMoves) {
		this.validMoves = validMoves;
	}
	
	public int[] getValidMoves() {
		return validMoves;
	}
}
