import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;

public class ClientUI extends JPanel {

	private JButton[][] board = new JButton[8][8];
	//private BoardSquare[][] board = new BoardSquare[8][8];
	private Border gridBorder = new LineBorder(Color.WHITE, 1);
	private Color reversiGreen = new Color(57, 126, 88);
	private Color lightBlack = new Color(32, 32, 32);
	private Color highlightYellow = new Color(255,255,102);
	private Font titleFont = new Font("Ariel", Font.BOLD, 40);
	private Font playerFont = new Font("Ariel", Font.BOLD, 25);
	private Font scoreFont = new Font("Ariel", Font.BOLD, 20);
	private Font winnerFont = new Font("Ariel", Font.BOLD, 20);
	private GameLogic logic;
	private int playerColor = logic.getPlayerColor();
	private int opponentColor = logic.getOpponentColor();
	private ImageIcon blackPiece;
	private ImageIcon whitePiece;
	private ImageIcon playerPiece;
	private ImageIcon opponentPiece;
	private JLabel scoreLabel1;
	private JLabel scoreLabel2;
	private JLabel infoLabel;
	private int whiteScore = 2;
	private int blackScore = 2;
	private boolean myTurn;
	private GameState currentState;
	private boolean iHaveNoMoves = false;
	private boolean opponentHasNoMoves = false;
	//private static PlayerClient client = new PlayerClient("172.17.152.133", 7077, new ClientUI(o));
	private ObjectOutputStream oos;

	public ClientUI(ObjectOutputStream oos, int clientType) {
		this.oos = oos;
		if(clientType == 1 || clientType == 2) {
			logic = new GameLogic(clientType);
		}
		Image blackImg = null;
		Image whiteImg = null;
		try {
			blackImg = ImageIO.read(ClientUI.class.getResource("res/blackpiece.png"));
			whiteImg = ImageIO.read(ClientUI.class.getResource("res/whitepiece.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		blackPiece = new ImageIcon(blackImg.getScaledInstance(43, 43, Image.SCALE_SMOOTH));
		whitePiece = new ImageIcon(whiteImg.getScaledInstance(43, 43, Image.SCALE_SMOOTH));
		
		if(playerColor == 1) {
			playerPiece = whitePiece;
			opponentPiece = blackPiece;
		}
		else {
			playerPiece = blackPiece;
			opponentPiece = whitePiece;
			
		}

	}
	
	public void createAndShowGUI() {
		

		JFrame frame = new JFrame("Reversi");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setBackground(lightBlack);

		MigLayout mig = new MigLayout("","[center]","");
		frame.setLayout(mig);

		//Title
		JLabel title = new JLabel("REVERSI");
		title.setForeground(Color.WHITE);
		title.setFont(titleFont);

		//Reversi Board
		JPanel boardPanel = new JPanel(new GridLayout(8,8,0,0));
		boardPanel.setPreferredSize(new Dimension(400,400));
		boardPanel.setBorder(gridBorder);

		for(int i=0; i<8; i++) {
			for(int j=0; j<8; j++) {
				board[i][j] = new JButton();
				board[i][j].setBackground(reversiGreen);
				board[i][j].setOpaque(true);
				//board[i][j].addMouseListener(highlight);
				//Top row
				if (i==0) {
					if (j==0) {
						//Top left corner
						board[i][j].setBorder(BorderFactory.createMatteBorder(4, 4, 2, 2, Color.BLACK));
					}
					else if(j==7) {
						//Top right corner
						board[i][j].setBorder(BorderFactory.createMatteBorder(4, 0, 2, 4, Color.BLACK));
					}
					else {
						// Top edge
						board[i][j].setBorder(BorderFactory.createMatteBorder(4, 0, 2, 2, Color.BLACK));
					}
				}
				//Bottom row
				else if(i==7) {
					if(j==0) {
						//Bottom left corner
						board[i][j].setBorder(BorderFactory.createMatteBorder(0, 4, 4, 2, Color.BLACK));
					}
					else if(j==7) {
						//Bottom right corner
						board[i][j].setBorder(BorderFactory.createMatteBorder(0, 0, 4, 4, Color.BLACK));
					}
					else {
						board[i][j].setBorder(BorderFactory.createMatteBorder(0, 0, 4, 2, Color.BLACK));
					}
				}
				else {
					if (j==0) {
						//Left-hand edge
						board[i][j].setBorder(BorderFactory.createMatteBorder(0, 4, 2, 2, Color.BLACK));
					}
					else if(j==7) {
						//Right-hand edge
						board[i][j].setBorder(BorderFactory.createMatteBorder(0, 0, 2, 4, Color.BLACK));
					}
					else {
						//Non-edge buttons
						board[i][j].setBorder(BorderFactory.createMatteBorder(0, 0, 2, 2, Color.BLACK));
					}
				}


				boardPanel.add(board[i][j]);
			}
		}

		/*
		board[3][3].setIcon(whitePiece);
		board[4][4].setIcon(whitePiece);
		board[3][4].setIcon(blackPiece);
		board[4][3].setIcon(blackPiece);
		*/
		
		board[0][0].setIcon(blackPiece);
		//board[0][1].setIcon(whitePiece);
		board[0][2].setIcon(whitePiece);
		board[0][3].setIcon(blackPiece);
		board[0][4].setIcon(whitePiece);
		board[0][5].setIcon(whitePiece);
		board[0][6].setIcon(blackPiece);
		board[0][7].setIcon(blackPiece);

		//board[1][0].setIcon(blackPiece);
		board[1][1].setIcon(whitePiece);//board[1][1].setIcon(whitePiece);
		board[1][2].setIcon(whitePiece);//board[1][2].setIcon(whitePiece);
		board[1][3].setIcon(blackPiece);
		board[1][4].setIcon(whitePiece);
		board[1][5].setIcon(whitePiece);
		board[1][6].setIcon(whitePiece);
		board[1][7].setIcon(whitePiece);

		board[2][0].setIcon(whitePiece);
		board[2][1].setIcon(whitePiece);//board[2][1].setIcon(whitePiece);
		board[2][2].setIcon(blackPiece);
		board[2][3].setIcon(whitePiece);
		board[2][4].setIcon(blackPiece);
		board[2][5].setIcon(whitePiece);
		board[2][6].setIcon(blackPiece);
		board[2][7].setIcon(whitePiece);

		board[3][0].setIcon(whitePiece);
		board[3][1].setIcon(whitePiece);
		board[3][2].setIcon(whitePiece);
		board[3][3].setIcon(blackPiece);
		board[3][4].setIcon(blackPiece);
		board[3][5].setIcon(whitePiece);
		board[3][6].setIcon(blackPiece);
		board[3][7].setIcon(blackPiece);

		board[4][0].setIcon(whitePiece);
		board[4][1].setIcon(whitePiece);
		board[4][2].setIcon(whitePiece);
		board[4][3].setIcon(whitePiece);
		board[4][4].setIcon(whitePiece);
		board[4][5].setIcon(blackPiece);
		board[4][6].setIcon(blackPiece);
		board[4][7].setIcon(blackPiece);

		board[5][0].setIcon(whitePiece);
		board[5][1].setIcon(whitePiece);
		board[5][2].setIcon(blackPiece);
		board[5][3].setIcon(whitePiece);
		board[5][4].setIcon(blackPiece);
		board[5][5].setIcon(blackPiece);
		board[5][6].setIcon(blackPiece);
		board[5][7].setIcon(blackPiece);

		board[6][0].setIcon(whitePiece);
		board[6][1].setIcon(whitePiece);
		board[6][2].setIcon(whitePiece);
		board[6][3].setIcon(blackPiece);
		board[6][4].setIcon(blackPiece);
		board[6][5].setIcon(blackPiece);
		board[6][6].setIcon(whitePiece);
		board[6][7].setIcon(blackPiece);

		board[7][0].setIcon(whitePiece);
		board[7][1].setIcon(whitePiece);
		board[7][2].setIcon(blackPiece);
		board[7][3].setIcon(blackPiece);
		board[7][4].setIcon(blackPiece);
		board[7][5].setIcon(blackPiece);
		board[7][6].setIcon(blackPiece);
		board[7][7].setIcon(blackPiece);
			
		logic.checkPossibleMoves();
		if(playerColor == 1) {
			currentState = GameState.OPPONNENT_TURN;
			enableComponents(false);
			myTurn = false;
		}
		else {
			currentState = GameState.MY_TURN;
			myTurn = true;
		}

		for(int i=0; i<8; i++) {
			for(int j=0; j<8; j++) {
				
				final int row = i;
				final int col = j;
			
				board[i][j].addMouseListener(new MouseListener() {
					int[] validArray = logic.getValidMoves(row, col);
					@Override
					public void mouseEntered(MouseEvent e) {
						if(logic.getValidMoves(row,col) != null && myTurn == true) {
							e.getComponent().setBackground(highlightYellow);
						}
						else {
							return;
						}
						
					}
					
					@Override
					public void mouseExited(MouseEvent e) {
						if(logic.getValidMoves(row,col) != null) {
							e.getComponent().setBackground(reversiGreen);
						}
						else {
							return;
						}
					}

					@Override
					public void mouseClicked(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
				});
				
				board[i][j].addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {

							GameStateUpdate(row, col);
						
					}
				});      
			}
		}

		//Info Panel
		JPanel infoPanel = new JPanel();
		infoPanel.setSize(new Dimension(400,50));
		//MigLayout mig2 = new MigLayout("","[center]220[center]","");
		MigLayout mig2 = new MigLayout("",
				"[100,center][190,center][100,center]",
				"[][]");
		infoPanel.setLayout(mig2);
		infoPanel.setBackground(lightBlack);
		JLabel p1Label = new JLabel("<HTML><U>Black</U></HTML>");
		p1Label.setForeground(Color.WHITE);
		p1Label.setFont(playerFont);
		scoreLabel1 = new JLabel("2");
		scoreLabel1.setForeground(Color.WHITE);
		scoreLabel1.setFont(scoreFont);
		JLabel p2Label = new JLabel("<HTML><U>White</U></HTML>");
		p2Label.setForeground(Color.WHITE);
		p2Label.setFont(playerFont);
		
		scoreLabel2 = new JLabel("2");	
		scoreLabel2.setForeground(Color.WHITE);
		scoreLabel2.setFont(scoreFont);
		
		infoLabel = new JLabel();
		infoLabel.setForeground(Color.WHITE);
		infoLabel.setFont(winnerFont);

		infoPanel.add(p1Label);
		infoPanel.add(infoLabel, "span 1 2");
		infoPanel.add(p2Label, "wrap");
		infoPanel.add(scoreLabel1);
		infoPanel.add(scoreLabel2);

		frame.add(title, "wrap");
		frame.add(boardPanel, "wrap");
		frame.add(infoPanel);
		frame.setVisible(true);
		frame.pack();
	}
	

	
	public void setGamePieces(int[] message, int player) {
		ImageIcon flipPieces = null;
		if(player == 1) flipPieces = opponentPiece;
		else flipPieces = playerPiece;
			for(int i=0; i<message.length; i+=2) {
				board[message[i]][message[i+1]].setIcon(flipPieces);
				board[message[i]][message[i+1]].setDisabledIcon(flipPieces);
			}
	}

	public void enableComponents(boolean enable) {
		this.myTurn = enable;
		if(enable == true) {
			for(int i=0; i<8; i++) {
				for(int j=0; j<8; j++) {
					board[i][j].setEnabled(true);	
				}
			}
		}
		
		else {
			for(int i=0; i<8; i++) {
				for(int j=0; j<8; j++) {
					if(board[i][j].getIcon() == whitePiece) {
						board[i][j].setDisabledIcon(whitePiece);
					}
					else if(board[i][j].getIcon() == blackPiece) {
						board[i][j].setDisabledIcon(blackPiece);
					}
					board[i][j].setEnabled(false);
				}
			}
		}
	}
	
	public void setEnabled(boolean enable) {
		this.myTurn = enable;
	}
	
	public boolean getEnabled() {
		return myTurn;
	}



	public void setOpponentMove(int[] opponentMoves) {
		setGamePieces(opponentMoves, 1);
		logic.setScore(opponentMoves.length/2, true);
		logic.updateBoard(opponentMoves, opponentColor);
		if(opponentMoves.length%2 != 0) GameStateUpdate(0, 0);	//you have no moves so still opponents turn
		else GameStateUpdate(1, 0);
	}

	public void setOpponentMove(int[] opponentMoves, String gameOver) {
		/*
		setGamePieces(opponentMoves, 1);
		setEnabled(false);
		logic.setScore(opponentMoves.length/2, true);
		logic.updateBoard(opponentMoves, opponentColor);
		infoLabel.setText(gameOver);
		//GameStateUpdate(0, 0);
		 */
		setGamePieces(opponentMoves, 1);
		logic.setScore(opponentMoves.length/2, true);
		logic.updateBoard(opponentMoves, opponentColor);
		infoLabel.setText(gameOver);
		GameStateUpdate(0, 0);	//you have no moves so still opponents turn
	
	}

	public enum GameState {
		MY_TURN, OPPONNENT_TURN;
	}

	public void GameStateUpdate(int row, int col) {

		// handle update
		switch(currentState) {
		case MY_TURN:
			//Place pieces for user move
			int[] validArray = logic.getValidMoves(row, col);
			if(validArray != null) {
				setGamePieces(validArray, 2);
				logic.setScore(validArray.length/2, false);
				scoreLabel1.setText(Integer.toString(logic.getBlackScore()));
				scoreLabel2.setText(Integer.toString(logic.getWhiteScore()));
				logic.updateBoard(validArray, playerColor);
				
				//If you win or lose the game
				if(logic.getBlackScore() + logic.getWhiteScore() == 64) {
					String gameOver1 = null;
					if(logic.getBlackScore() > logic.getWhiteScore()) {
						gameOver1 = "BLACK\nWINS";
						infoLabel.setText("BLACK\nWINS");
					}
					else if (logic.getBlackScore() < logic.getWhiteScore()) {
						gameOver1 = "WHITE\nWINS";
						infoLabel.setText("WHITE\nWINS");
					}
					else {
						gameOver1 = "TIE";
						infoLabel.setText("TIE");
					}
					try {
						oos.writeObject(gameOver1);
						oos.flush();  
						oos.writeObject(validArray);
						oos.flush();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					enableComponents(false);
					break;
				}
				
				//If Opponent has no legal moves
				else if(logic.checkOpponentMoves() == 0) {
					logic.checkPossibleMoves();
					//Both you and opponent have no legal moves so game over
					if(logic.getPossibleMoves() == 0) {
						String gameOver1 = null;
						if (logic.getBlackScore()==logic.getWhiteScore()) {
							gameOver1 = "<html><div style=\"text-align: center;\">" + "NO MOVES LEFT<br>TIE GAME" + "</html>";
							infoLabel.setText("<html><div style=\"text-align: center;\">" + "NO MOVES LEFT<br>TIE GAME" + "</html>");	
						}
						else if(playerColor == 1) {
							if(logic.getBlackScore()>logic.getWhiteScore()) {
								gameOver1 = "<html><div style=\"text-align: center;\">" + "NO MOVES LEFT<br>YOU LOSE" + "</html>";
								infoLabel.setText("<html><div style=\"text-align: center;\">" + "NO MOVES LEFT<br>YOU LOSE" + "</html>");
							}
							else {
								gameOver1 = "<html><div style=\"text-align: center;\">" + "NO MOVES LEFT<br>YOU WIN" + "</html>";
								infoLabel.setText("<html><div style=\"text-align: center;\">" + "NO MOVES LEFT<br>YOU WIN" + "</html>");
							}
						}
						else {
							if(logic.getBlackScore()>logic.getWhiteScore()) {
								gameOver1 = "<html><div style=\"text-align: center;\">" + "NO MOVES LEFT<br>YOU WIN" + "</html>";
								infoLabel.setText("<html><div style=\"text-align: center;\">" + "NO MOVES LEFT<br>YOU WIN" + "</html>");
							}
							else {
								gameOver1 = "<html><div style=\"text-align: center;\">" + "NO MOVES LEFT<br>YOU LOSE" + "</html>";
								infoLabel.setText("<html><div style=\"text-align: center;\">" + "NO MOVES LEFT<br>YOU LOSE" + "</html>");
							}
						}
						try {
							oos.writeObject(gameOver1);
							oos.flush();  
							oos.writeObject(validArray);
							oos.flush();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						enableComponents(false);
						break;//popup message for endgame
					}
					//Opponent has no legal moves so still your turn
					else {
						infoLabel.setText("OPPONENT HAS NO LEGAL MOVE\n         YOUR TURN");
						int[] noMoves = new int[validArray.length+1];
						for(int i=0; i<validArray.length; i++) {
							noMoves[i] = validArray[i];
						}
						try {
							oos.writeObject(noMoves);
							oos.flush();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;//you get another turn
					}
				}

				//Normal move
				else {
					try {
						oos.writeObject(validArray);
						oos.flush();  
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					enableComponents(false);
					currentState = GameState.OPPONNENT_TURN;
					break;
				}
			}

		case OPPONNENT_TURN:
			if(row == 0) {	//no possible moves this turn/GAME OVER
				scoreLabel1.setText(Integer.toString(logic.getBlackScore()));
				scoreLabel2.setText(Integer.toString(logic.getWhiteScore()));
				break;
			}
			else {	//Normal Move
				logic.checkPossibleMoves();
				scoreLabel1.setText(Integer.toString(logic.getBlackScore()));
				scoreLabel2.setText(Integer.toString(logic.getWhiteScore()));
				enableComponents(true);
				currentState = GameState.MY_TURN;
				break;
			}

		}
	}

	public static void main(String[] args) throws IOException {
		ClientUI ui = new ClientUI(null);
		ui.createAndShowGUI();
	}

}

