import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

public class PlayerClient implements Runnable{

	private static String host = "172.17.152.133";
	private static int port = 7077;
	private Socket socket;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private Thread outputThread;
	private static ClientUI gui;

	public PlayerClient(String host, int port, ClientUI gui) {
		this.host = host;
		this.port = port;
		this.gui = gui;
	}

	@Override
	public void run() {

		try {
			socket = new Socket(host, port);
			System.out.println("CONNECTED");
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());
			
			int clientType = ois.readInt();

			gui = new ClientUI(oos, clientType);
			gui.createAndShowGUI();

			while(true) {
				Object receivedMessage = ois.readObject();
				if(receivedMessage instanceof String) {
					Object receivedMessage2 = ois.readObject();
					String gameOver = (String) receivedMessage;
					int[] opponentMoves = (int[]) receivedMessage2;
					gui.setOpponentMove(opponentMoves, gameOver);
				}
				else {
					int[] opponentMoves = (int[]) receivedMessage;
					gui.setOpponentMove(opponentMoves);
				}
			}

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int[] readMessage() {
		int[] array = null;
		try {
			array = (int[])ois.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return array; 
	}

	public void sendMessage(int[] message) {
		try {
			oos.writeObject(message);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
public static void main(String[] args) throws IOException {
	//new PlayerClient(host, port, gui).startClient();

	(new Thread(new PlayerClient(host, port, gui))).start();

	}
	 
}
