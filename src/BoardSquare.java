import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;

public class BoardSquare extends JButton {//implements ActionListener{
	
	private JButton button;
	private int status;
	private int[][] boardStatus;
	private Image blackImg = ImageIO.read(ClientUI.class.getResource("res/blackpiece.png"));
	private Image whiteImg = ImageIO.read(ClientUI.class.getResource("res/whitepiece.png"));
	private ImageIcon blackPiece = new ImageIcon(blackImg.getScaledInstance(43, 43, Image.SCALE_SMOOTH));
	private ImageIcon whitePiece = new ImageIcon(whiteImg.getScaledInstance(43, 43, Image.SCALE_SMOOTH));
	private HighlightValid highlight = new HighlightValid();
	
	public BoardSquare() throws IOException {
		this.button = new JButton();
		this.status = 0;
		
		this.addMouseListener(highlight); 

	}
	
	public void setValid(boolean valid) {
		highlight.setValid(valid);
	}
	
	
	/*
	board[i][j].addMouseListener(new java.awt.event.MouseAdapter() {
	    public void mouseEntered(java.awt.event.MouseEvent evt) {
	    	if(logic.getValidMoves(row, col) != null)
	    	board[row][col].setBackground(highlightYellow);
	    }

	    public void mouseExited(java.awt.event.MouseEvent evt) {
	    	board[row][col].setBackground(reversiGreen);
	    }
	});
	*/
	
	@Override
	public void setEnabled(boolean enable) {
		if(enable == false) {
			super.setEnabled(false);
			setDisabledIcon(getIcon());
		}
		else {
			super.setEnabled(true);
		}
	}
	
}

class HighlightValid implements MouseListener {
	
	private boolean valid = false;
	private Color highlightYellow = new Color(255,255,102);
	private Color reversiGreen = new Color(57, 126, 88);
	
	public HighlightValid() {

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if(valid == false) {
			return;
		}
		else {
			e.getComponent().setBackground(highlightYellow);
		}
		
	}
	
	public void setValid(boolean valid) {
		this.valid = valid;
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		e.getComponent().setBackground(reversiGreen);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}







